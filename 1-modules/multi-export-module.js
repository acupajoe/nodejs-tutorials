/**
 * A multiple export module
 *
 * Instead of declaring properties outside of the module.exports call,
 * we can assign them directly.
 */

var hiddenAttr = "I am hidden"

module.exports = {
  helloWorld: function () {
    console.log('hello world')
  },
  props: {
    states: ['stopped', 'running', 'paused'],
    playTime: 1052
  }
}
