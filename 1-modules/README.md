# Node Modules

Node allows the importing of code via `modules`. Instead of relying on the browser to import code, node explicitly imports it. 

Importing code is called `require` in available node versions.


### Requiring Code
```javascript
var http = require('http')
```

The `http` require is from a node `module`. It is part of what node installs on its system, so it does not have any file system prefix. Modules that are installed via `npm install module` or `yarn add module` (and are placed in the `node_modules` folder) can _also_ be required without a filesystem prefix.

(Note that `node_modules` is searched for in parent directories and the global module directory)


### Custom Modules
```javascript
var customImport = require('./customImport')
var aliasedImport = require('./customImport')
```

This is an import for our code, `require` will return whatever `module.exports` is set to in that file. 

- A `require` call for custom code **MUST** use a file system prefix.)
- `require` can be used without suffixing with `.js`
- `require` can import `.json` into your code 

Naming doesn't matter except for your own readability.
