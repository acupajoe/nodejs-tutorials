var multipleExports = require('./multi-export-module')
var singleExport = require('./single-export-module')
var constants = require('./constants')

console.log(constants.colors.black)

// multipleExports is assigned an object that we can access
multipleExports.helloWorld()
console.log(multipleExports.props.states)

// singleExport is basically an *aliased* function
singleExport('output')
