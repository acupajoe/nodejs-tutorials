// First we need to import or 'require' the http module
var http = require('http')

/**
 * Option 1: extract a listener to a separate function
 */
var listener = function (request, response) {
  // For Post requests
  console.log(request.body)
  // For Query strings
  console.log(request.query)

  var random = Math.random()

  response.header = 200
  response.send('Sent from an extracted function: ' + random)
}

var extractedServer = http.createServer(listener)