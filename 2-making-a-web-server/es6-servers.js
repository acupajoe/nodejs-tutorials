const http = require('http')

module.exports = http.createServer((request, response) => {
  // For Post requests
  console.log(request.body)
  // For Query strings
  console.log(request.query)

  let random = Math.random()

  response.header = 200
  response.send('Sent from an arrow function: ' + random)
})
