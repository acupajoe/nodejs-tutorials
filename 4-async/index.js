const Person = require('./person')

var array = [new Person(), new Person()]

console.log(`Started at: ${new Date().toTimeString()}`)

let people = [new Person(), new Person(), new Person(), new Person()]
let delayed = () => {
  return new Promise((resolve, reject) => {
    //Fake a delay
    setTimeout(() => resolve({data: true}), 2000)
  })
}

console.log('Before Delay Called')

delayed()
  .then(data => {
    console.log(data)
    return {passedAlongData: true}
  })
  .then(data => {
    console.log(data)
  })
  .then(() => Promise.all(people.map(person => person.getInfo())))
  .then(() => {
    console.log(people)
    console.log(`Finished at: ${new Date().toTimeString()}`)
  })

console.log('After Delay Called')
console.log(people)
