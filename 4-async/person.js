let names = ['Joshua', 'David', 'Marco', 'John', 'Jim', 'Kayla', 'Doris', 'Maddie']

class Person {
  getInfo() {
    return new Promise((resolve, reject) => {
      this.name = names[Math.floor(Math.random() * names.length)]
      setTimeout(() => resolve(this), Math.random() * (200 - 1000) + 2000)
    })
  }
}

module.exports = Person